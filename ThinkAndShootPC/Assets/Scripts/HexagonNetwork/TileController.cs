﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileController : MonoBehaviour
{
    [SerializeField]
    private Color highlightedColor = Color.yellow;
    [SerializeField]
    private Color normalColor = Color.black;
    [SerializeField]
    private Color blockedColor = Color.red;
    [SerializeField]
    private Color availableColor = Color.green;
    [SerializeField]
    private SpriteRenderer hexFill;
    [SerializeField]
    private const int ignoreRaycastLayer = 2;
    [SerializeField]
    private const int hexNetworkLayer = 8;
    [SerializeField]
    private List<GameObject> boxCollidersGOs;

    public List<TileController> adjacentFields = new List<TileController>();

    private bool isPassable = true;
    private bool wasUsed;

    public bool IsPassable
    {
        get => isPassable;
        set
        {
            isPassable = value;
            if(!value)
            {
                MarkAsBlocked();
                ChangeLayer(boxCollidersGOs, ignoreRaycastLayer);
            }
            else
            {
                BackToNormalTile();
                ChangeLayer(boxCollidersGOs, hexNetworkLayer);
            }
        }
    }

    public void HighlightTile()
    {
        hexFill.color = highlightedColor;
    }

    public void MarkAsAvailable()
    {
        hexFill.color = availableColor;
    }

    public void BackToNormalTile()
    {
        hexFill.color = normalColor;
    }

    public void MarkAsBlocked()
    {
        hexFill.color = blockedColor;
    }

    public Vector3 GetPosition()
    {
        return this.transform.position;
    }

    private void ChangeLayer(List<GameObject> collidersGOs, int layerID)
    {
        foreach(GameObject go in collidersGOs)
        {
            go.layer = layerID;
        }
    }

}
