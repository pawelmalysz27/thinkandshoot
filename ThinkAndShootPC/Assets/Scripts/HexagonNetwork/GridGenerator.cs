﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GridGenerator : MonoBehaviour
{
    public GameObject hexTile;
    public Vector3 center = new Vector3(0,0,0);
    public Transform hexTransform;
    public GameObject[,] tilesArray;
    private float a = 0.50000000f;
    private float high;
    int startX = 0;
    int startY = 0;
    int endX = 0;
    int endY = 0;
    int tmp = 0;


    public void GenerateGrid(int horizontal, int vertical)
    {
        tilesArray = new GameObject[horizontal, vertical];

        if (hexTile)
        {
            center = new Vector3(0, 0, 0);
            hexTransform.position = center;

            high = a * Mathf.Sqrt(3f);
            Debug.Log(high);
            for (int v = 0; v < vertical; v++)
            {
                for (int h = 0; h < horizontal; h++)
                {
                    if (h == 0)
                    {
                        hexTransform.position = new Vector3(hexTransform.position.x, transform.position.y, hexTransform.position.z);
                        center = hexTransform.position;
                    }
                    else
                    {
                        hexTransform.position = new Vector3(hexTransform.position.x + high, transform.position.y, hexTransform.position.z);
                    }
                    tilesArray[v, h] = GameObject.Instantiate(hexTile.gameObject, hexTransform.position, hexTile.transform.rotation, this.gameObject.transform);
                    tilesArray[v, h].name = "Tile (" + v + "," + h + ")";
                }
                hexTransform.position = new Vector3(center.x, hexTransform.position.y, center.z);

                if (v % 2 != 0)
                    hexTransform.position = new Vector3(hexTransform.position.x + high / 2, transform.position.y, hexTransform.position.z + a + (a / 2));
                else
                    hexTransform.position = new Vector3(hexTransform.position.x - high / 2, transform.position.y, hexTransform.position.z + a + (a / 2));
            }

            //RenameTiles();
            SetAllAdjacentFields();
            //DebugLogArray();
        }
        else
            Debug.Log("There is no Tile to create Grid.");
    }

    private void RenameTiles()
    {
        
        TileController[] tiles = GetComponentsInChildren<TileController>();
        foreach(TileController t in tiles)
        {
            GameObject go = t.gameObject;
            go.name = "Tile(" + go.transform.position.x + "," + go.transform.position.z + ")";
        }
    }

    private void DebugLogArray()
    {
        string str = "";
        for(int i=0;i<tilesArray.GetLength(0); i++)
        {
            for(int j=0;j<tilesArray.GetLength(1);j++)
            {
                str += tilesArray[i, j].name + "              ";
            }
            Debug.Log(str);
            str = "";
        }
    }

    private void SetAllAdjacentFields()
    {
        for (int i = 0; i < tilesArray.GetLength(0); i++)
        {
            for (int j = 0; j < tilesArray.GetLength(1); j++)
            {
                SetAdjacentFields(i, j);
            }
        }
    }

    private void SetAdjacentFields(int x, int y)
    {
        startX = 0;
        startY = 0;
        endX = 0;
        endY = 0;

        if (x == 0)
        {
            startX = 0;
        }
        else
        {
            startX = x - 1;
        }

        if(x == tilesArray.GetLength(0)-1)
        {
            endX = x;
        }
        else
        {
            endX = x + 1;
        }

        if(x == 0)
        {
            tmp = 1;
        }
        else
        {
            tmp = 0;
        }
        

        for (int i =startX; i <= endX; i++)
        {
            if (x % 2 != 0)
            {
                SetAdjacentFieldsInOddRow(i, x, y);
            }
            else
            {
                SetAdjacentFieldsInEvenRow(i, x, y);
            }
            tmp++;
        }
    }

    private void SetAdjacentFieldsInOddRow(int i, int x, int y)
    {
        if (y == 0)
        {
            startY = 0;
        }
        else
        {
            startY = y - 1;
        }

        if (tmp % 2 == 0)
        {
            endY = y;
        }
        else if (y != tilesArray.GetLength(1) - 1)
        {
            endY = y + 1;
        }

        for (int j = startY; j <= endY; j++)
        {
            if (i == x && j == y)
            {

            }
            else
            {
                tilesArray[x, y].GetComponent<TileController>().adjacentFields.Add(tilesArray[i, j].GetComponent<TileController>());
            }
        }
    }

    private void SetAdjacentFieldsInEvenRow(int i, int x, int y)
    {
        if (tmp % 2 == 0)
        {
            startY = y;
        }
        else if (y != 0)
        {
            startY = y - 1;
            Debug.Log(" asd " + startY);
        }

        if (y == tilesArray.GetLength(1) - 1)
        {
            endY = y;
        }
        else
        {
            endY = y + 1;
        }

        for (int j = startY; j <= endY; j++)
        {
            if (i == x && j == y)
            {

            }
            else
                tilesArray[x, y].GetComponent<TileController>().adjacentFields.Add(tilesArray[i, j].GetComponent<TileController>());

        }
    }


}



[CustomEditor(typeof(GridGenerator))]
public class GridGeneratorEditor : Editor
{
    bool btn = false;
    int x = 10;
    int y = 10;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        GridGenerator myTarget = (GridGenerator)target;


        x = EditorGUILayout.IntField("Horizontal: ", x);
        y = EditorGUILayout.IntField("Vertical: ", y);
        if (!btn)
            btn = GUILayout.Button("Generate Grid");
        else
        {
            myTarget.GenerateGrid(x, y);
            Debug.Log("Generated Grid " + x + "/" + y);
            btn = false;
        }
    }

}

