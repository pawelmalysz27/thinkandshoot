﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileBlocker : MonoBehaviour
{
    [SerializeField]
    private TileController tileController;

    private const string obstacleTag = "Obstacle";

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == obstacleTag)
        {
            tileController.IsPassable = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == obstacleTag)
        {
            tileController.IsPassable = true;
        }
    }
}
