﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseHoverController : MonoBehaviour
{
    [SerializeField]
    private TileController tileController;

    private void OnMouseEnter()
    {
        tileController.MarkAsAvailable();
    }

    private void OnMouseExit()
    {
        tileController.BackToNormalTile();
    }
}
