﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField]
    private PlayerController playerController;

    public Camera mainCamera;
    RaycastHit rayHit;

    public RaycastHit RayHit
    {
        get
        {
            return rayHit;
        }

        set
        {
            rayHit = value;
        }
    }

    private void Update()
    {
        if(Input.GetMouseButtonUp(0))
        {
            RayHit = GetRaycast();

            if(rayHit.transform.CompareTag("Character"))
            {
                CharacterInfo characterInfo = RayHit.transform.GetComponent<CharacterInfo>();
                playerController.SelectCharacter(characterInfo.CharacterID);
            }
        }
    }

    public RaycastHit GetRaycast()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out hit, Mathf.Infinity);

        return hit;
    }
}
