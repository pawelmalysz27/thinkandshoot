﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathIndicator : MonoBehaviour
{
    [SerializeField]
    private LineRenderer lineRenderer;
    [SerializeField]
    private GameObject cross;

    public void DrawPath(Vector3[] routePoints)
    {
        if(!lineRenderer.gameObject.activeSelf)
        {
            SetPathIndicatorActive(true);
        }
        lineRenderer.positionCount = routePoints.Length;
        lineRenderer.SetPositions(routePoints);
        SetCross(routePoints[routePoints.Length-1]);
    }

    public void SetPathIndicatorActive(bool active)
    {
        lineRenderer.gameObject.SetActive(active);

        if(!active)
        {
            cross.SetActive(false);
        }
    }

    private void SetCross(Vector3 position)
    {
        cross.transform.position = position;
        cross.SetActive(true);
    }

}
