﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconController : MonoBehaviour
{
    [SerializeField]
    private float rotationSpeed = 5f;

    private void Update()
    {
        transform.Rotate(1f, 1f, 1f);
    }

}
