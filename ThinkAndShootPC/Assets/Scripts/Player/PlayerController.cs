﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private List<CharacterMainController> characters;
    [SerializeField]
    private InputController inputController;

    private CharacterMainController activeCharacter;

    private RaycastHit rayHit;

    public RaycastHit RayHit
    {
        get => rayHit;
        set
        {
            rayHit = value;
            activeCharacter.SetTargetTile(rayHit.transform);
        }
    }

    private void Update()
    {
        if (activeCharacter != null)
        {
            if (Input.GetMouseButtonUp(0))
            {
                RayHit = inputController.GetRaycast();
            }
        }
    }

    public void SelectCharacter(int characterID)
    {
        for (int i = 0; i < characters.Count; i++)
        {
            if (i == characterID)
            {
                characters[i].IsActive = true;
                characters[i].activityIcon.SetActive(true);
                activeCharacter = characters[i];
            }
            else
            {
                characters[i].IsActive = false;
                characters[i].activityIcon.SetActive(false);
            }
        }

        Debug.Log("Selected other character!");
    }

}
