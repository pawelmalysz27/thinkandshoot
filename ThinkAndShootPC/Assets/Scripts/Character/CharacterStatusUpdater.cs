﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStatusUpdater : MonoBehaviour
{
    private const int numberOfStaminaCells = 12;

    [SerializeField]
    private GameObject healthBar;
    [SerializeField]
    private GameObject staminaBar;

    private Text hpText;
    private Image hp;
    private RectTransform hpRectTransform;
    private RectTransform staminaRectTransform;

    private void Awake()
    {
        hpRectTransform = healthBar.GetComponent<RectTransform>();
        staminaRectTransform = staminaBar.GetComponent<RectTransform>();
    }

    public void SetHpBarSize(float value)
    {
        hpRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, value);
    }

    public void SetStaminaBarSize(int value)
    {
        float width = CalculateSizeOfStaminaBar(value);
        staminaRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
    }

    private float CalculateSizeOfStaminaBar(int value)
    {
        float size = (float)(value / 12f) * 100;
        return size;
    }
}
