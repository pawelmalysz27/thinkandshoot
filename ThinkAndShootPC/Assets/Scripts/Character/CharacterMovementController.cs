﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementController : MonoBehaviour
{
    [SerializeField]
    private float runSpeed = 2f;
    [SerializeField]
    private float rotationSpeed = 3f;
    [SerializeField]
    private CharacterController characterController;
    [SerializeField]
    private CharacterAnimationController characterAnimationController;
    [SerializeField]
    private RouteManager routeManager;
    [SerializeField]
    private PathIndicator pathIndicator;

    private RaycastHit rayHit;
    private bool isMoving;
    //private bool isActive;
    private TileController startingTile;
    private TileController targetTile;
    private List<Vector3> routePoints;
    private int counter;
    private Vector3 nextRoutePos;

    public bool finishedRoute;

    public TileController TargetTile
    {
        get => targetTile;
        set
        {
            targetTile = value;
            RoutePoints = routeManager.GetRoute(targetTile);
            DrawPath(RoutePoints);
        }
    }

    public List<Vector3> RoutePoints
    {
        get => routePoints;
        set
        {
            routePoints = value;
            IsMoving = true;
            counter = 0;
            nextRoutePos = transform.position;
        }
    }

    public bool IsMoving
    {
        get => isMoving;
        set
        {
            isMoving = value;
            characterAnimationController.PlayRunningAnimation(isMoving);
            if (isMoving)
            {
                finishedRoute = false;
            }
            else
            {
                finishedRoute = true;
            }
        }
    }

    //public bool IsActive { get => isActive; set => isActive = value; }
    

    void Update()
    {
       
        if(IsMoving)
        {
            FollowRoute();
        }
    }

    private void FollowRoute()
    {
        if (Vector3.Distance(transform.position, nextRoutePos) < 0.05f)
        {
            if (counter < RoutePoints.Count)
            {
                nextRoutePos = GetNextRoutePoint();
            }
            else
            {
                RoutePoints.Clear();
                IsMoving = false;
                pathIndicator.SetPathIndicatorActive(false);
            }
        }

        RotateToPosition(nextRoutePos);
        MoveToPosition(nextRoutePos);
    }

    private void RotateToPosition(Vector3 position)
    {
        position.y = transform.position.y;
        Quaternion targetRotation = Quaternion.LookRotation(position - transform.position, Vector3.up);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);
    }

    Vector3 GetNextRoutePoint()
    {
        return RoutePoints[counter++];
    }

    private void MoveToPosition(Vector3 moveDestination)
    {
        characterController.Move(Vector3.Normalize(moveDestination - transform.position) * runSpeed * Time.deltaTime);

        if (Vector3.Distance(transform.position, moveDestination) < 0.05f && IsMoving)
        {
            transform.position = moveDestination;
        }
    }

    private void DrawPath(List<Vector3> routePoints)
    {
        Vector3[] _routePoints = new Vector3[routePoints.Count + 1];
        for(int i = 0; i< _routePoints.Length; i++)
        {
            if(i == 0)
            {
                _routePoints[i] = transform.position;
            }
            else
            {
                _routePoints[i] = routePoints[i - 1];
            }
        }

        pathIndicator.DrawPath(_routePoints);
    }


}

