﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationController : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    public bool IsRunning
    {
        get
        {
            return animator.GetBool("isRunning");
        }
        set
        {
            animator.SetBool("isRunning", value);
        }
    }

    public void PlayRunningAnimation(bool isRunning)
    {
        if(isRunning)
        {
            IsRunning = true;
        }
        else
        {
            IsRunning = false;
        }
    }

}
