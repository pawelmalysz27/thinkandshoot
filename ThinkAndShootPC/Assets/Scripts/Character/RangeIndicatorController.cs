﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeIndicatorController : MonoBehaviour
{
    private const float rangeMultiplier = 0.88f; // 0.88 radius per 1 hexagon range
    private const string layerName = "HexagonPointers";
    private const int numberOfRowsToCheck = 2;

    private List<TileController> allTileControllers = new List<TileController>();
    

    private Collider[] GetTilesInRange(int range)
    {
        return Physics.OverlapSphere(transform.position, (float)(rangeMultiplier * range), LayerMask.GetMask(layerName), QueryTriggerInteraction.Collide);
    }

    public void ShowMovementRange(int range, RouteManager routeManager)
    {
        Collider[] colliders = GetTilesInRange(range);
        CheckOutsideTiles(range, colliders, routeManager); //6 because outside tiles - range: 1 - 6, 2 - 12, 3 - 18, 4 - 24 etc
    }

    public void CheckOutsideTiles(int movementRange, Collider[] colliders, RouteManager routeManager)
    {
        List<TileController> tileControllers = new List<TileController>();

        SetTilesToNormalColor();

        int tileRowsToCheck;
        if (movementRange > numberOfRowsToCheck)
        {
            tileRowsToCheck = movementRange - numberOfRowsToCheck;

            foreach (Collider col in colliders)
            {
                TileController tile = col.GetComponentInParent<TileController>();
                if (Vector3.Distance(transform.position, tile.GetPosition()) > (tileRowsToCheck) * rangeMultiplier)
                {
                    tileControllers.Add(tile);
                }
                if (tile.IsPassable)
                {
                    allTileControllers.Add(tile);
                }
            }
        }
        else
        {
            foreach (Collider col in colliders)
            {
                TileController tile = col.GetComponentInParent<TileController>();
                tileControllers.Add(tile);
                if (tile.IsPassable)
                {
                    allTileControllers.Add(tile);
                }
            }
        }

        foreach (TileController tileController in tileControllers)
        {
            if (tileController.IsPassable)
            {
                routeManager.CanGetRoute(tileController, movementRange);
            }
        }

    }

    private void SetTilesToNormalColor()
    {
        if (allTileControllers.Count > 1)
        {
            foreach (TileController tile in allTileControllers)
            {
                tile.BackToNormalTile();
            }
        }
        allTileControllers.Clear();
    }
}
