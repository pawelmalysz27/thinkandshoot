﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInfo : MonoBehaviour
{
    private float health;
    private int stamina;

    [SerializeField]
    private CharacterStatusUpdater characterStatusUpdater;

    public int characterID;

    public TileController actualTile;
    public TileController destinationTile;

    public int CharacterID
    {
        get => characterID;
        set => characterID = value;
    }

    public float Health
    {
        get => health;
        set
        {
            health = value;
            characterStatusUpdater.SetHpBarSize(health);
        }
    }

    public int Stamina
    {
        get => stamina;
        set
        {
            stamina = value;
            characterStatusUpdater.SetStaminaBarSize(stamina);
        }
    }

    public int MovementRange
    {
        get => (Stamina / 3);
    }
}
