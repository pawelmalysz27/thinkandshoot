﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RouteManager : MonoBehaviour
{
    public CharacterInfo characterInfo;
    
    public List<Vector3> routePoints = new List<Vector3>();

    public List<Vector3> GetRoute(TileController destinationTile)
    {
        Vector3 destination = destinationTile.GetPosition();
        TileController nextTile = null;

        while (!characterInfo.actualTile.name.Equals(destinationTile.name))
        {
            nextTile = GetNearestTile(destination, characterInfo.actualTile);
            characterInfo.actualTile = nextTile;
            characterInfo.actualTile.HighlightTile();

            routePoints.Add(characterInfo.actualTile.GetPosition());
        }
        Debug.Log("Znalazłem trasę! Posiada " + routePoints.Count + " punktów");

        return routePoints;
    }

    public bool CanGetRoute(TileController destinationTile, int movementRange)
    {
        Vector3 destination = destinationTile.GetPosition();
        TileController nextTile = null;
        TileController actualTile = characterInfo.actualTile;
        int counter = 0;

        while (!actualTile.name.Equals(destinationTile.name))
        {
            if (counter == movementRange)
            {
                return false;
            }

            nextTile = GetNearestTile(destination, actualTile);
            actualTile = nextTile;
            actualTile.MarkAsAvailable();
            
            counter++;
        }

        return true;
    }

    private TileController GetNearestTile(Vector3 destination, TileController actualTile)
    {
        TileController[] tiles = actualTile.adjacentFields.ToArray();
        TileController nearestTile = null;

        foreach(TileController t in tiles)
        {
            if (t.IsPassable && !routePoints.Contains(t.GetPosition()))
            {
                if (t.GetPosition() == destination)
                {
                    return t;
                }
                else
                {
                    if (nearestTile == null)
                    {
                        nearestTile = t;
                    }
                    else
                    {
                        float distance = Vector3.Distance(t.GetPosition(), destination);
                        float actualDistance = Vector3.Distance(nearestTile.GetPosition(), destination);


                        if (actualDistance > distance)
                        {
                            nearestTile = t;
                        }
                        else if (actualDistance == distance)
                        {
                            if (Random.Range(0, 2) == 1)
                            {
                                nearestTile = t;
                            }
                        }
                    }
                }
            }
        }

        return nearestTile;
    }

    private bool CheckTileIsNextTo(TileController tile, TileController targetTile)
    {
        foreach(TileController t in tile.adjacentFields)
        {
            if(t.gameObject.name == targetTile.gameObject.name)
            {
                Debug.Log("ZNALAZŁEM");
                return true;
            }
        }

        return false;
    }
}
