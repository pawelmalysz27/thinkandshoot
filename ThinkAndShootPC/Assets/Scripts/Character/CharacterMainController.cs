﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMainController : MonoBehaviour
{
    private const string hexFieldTag = "Hexagon";

    [SerializeField]
    private CharacterMovementController characterMovementController;
    [SerializeField]
    private CharacterAnimationController characterAnimationController;
    [SerializeField]
    private CharacterInfo characterInfo;
    [SerializeField]
    private RangeIndicatorController rangeIndicatorController;
    [SerializeField]
    private RouteManager routeManager;

    public GameObject activityIcon;
    public bool HitChar; //for tests
    public bool UseStamina;

    public bool isActive;

    public bool IsActive
    {
        get => isActive;
        set
        {
            isActive = value;
            if(isActive)
            {
                rangeIndicatorController.ShowMovementRange(characterInfo.MovementRange, routeManager);
            }
        }
    }

    private void Start()
    {
        characterInfo.Health = 100f;
        Debug.Log("asdas " + characterInfo.Health);
        characterInfo.Stamina = 12;
    }

    private void Update()
    {
        if(HitChar)
        {
            characterInfo.Health -= 10;
            HitChar = false;
        }
        if (UseStamina)
        {
            characterInfo.Stamina -= 1;
            UseStamina = false;
        }

        if(characterMovementController.finishedRoute && characterInfo.MovementRange > 0)
        {
            rangeIndicatorController.ShowMovementRange(characterInfo.MovementRange, routeManager);
        }
    }

    public void SetTargetTile(Transform hitObject)
    {
        if (IsActive)
        {
            if (CheckHexTag(hitObject))
            {
                characterMovementController.TargetTile = hitObject.GetComponentInParent<TileController>();                
            }
        }
    }

    private bool CheckHexTag(Transform tile)
    {
        if (tile.CompareTag(hexFieldTag))
        {
            return true;
        }

        return false;
    }
}
